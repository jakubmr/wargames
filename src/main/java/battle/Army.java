package battle;

import units.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * class representing an army
 */

public class Army {

    private String name;

    /**
     * arrayList containing the units of the army
     */
    private ArrayList<Unit> units;


    private static Random random = new Random();

    /**
     * Army constructor
     *
     * @param name
     */
    public Army(String name) {
        this.name = name;
        this.units = new ArrayList<>();
    }

    /**
     * extended Army constructor
     *
     * @param name
     * @param units
     */
    public Army(String name, ArrayList<Unit> units) {
        this.name = name;
        this.units = units;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    /**
     * adds a unit to the army list, if unit is recruited adds a copy
     *
     * @param unit
     */
    public void add(Unit unit) {
            this.units.add(unit.copyUnit());

    }

    /**
     * adds all units to army in a given list
     *
     * @param Units a list with Units
     */
    public void addAll(ArrayList<Unit> Units) {
        for (Unit unit : Units) {
            this.add(unit);
        }
    }

    public void addNumberOfUnits(int amount, Unit unit){
        for(int i = 0; i < amount; i++){
            this.add(unit);
        }
    }

    /**
     * removes given unit from the army
     *
     * @param unit
     */
    public void remove(Unit unit) {
        this.units.remove(unit);
    }

    /**
     * returns true if army has units left
     *
     * @return boolean
     */
    public boolean hasUnits() {
        if (this.units.size() < 1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * returns army units
     *
     * @return ArrayList<Unit>
     */
    public ArrayList<Unit> getAllUnits() {
        return this.units;
    }

    /**
     * return army size
     *
     * @return int
     */
    public int getSizeOfArmy(){
        return this.units.size();
    }

    /**
     * gets random unit from the army
     *
     * @return Unit
     */
    public Unit getRandom() {
        int index = random.nextInt(this.units.size());
        return this.units.get(index);
    }

    /**
     * clears the army
     */
    public void clearArmy(){
        this.getAllUnits().clear();
    }

    /**
     * getter for infantry units
     * @return infantry units in the army List
     */
    public List<Unit> getInfantryUnits(){
        return this.units.stream().filter(x -> x instanceof InfantryUnit).toList();
    }

    /**
     * getter for ranged units
     * @return ranged units in the army a List
     */
    public List<Unit> getRangedUnits(){
        return this.units.stream().filter(x -> x instanceof RangedUnit).toList();
    }

    /**
     * getter for cavalry units, excluding commander units
     * @return cavalry units in the army as a List
     */
    public List<Unit> getCavalryUnits(){
        return this.units.stream().filter(x -> x.getClass().equals(CavalryUnit.class)).toList();

    }

    /**
     * getter for commander units
     * @return commander units in the army as List
     */
    public List<Unit> getCommanderUnits(){
        return this.units.stream().filter(x -> x instanceof CommanderUnit).toList();
    }

    /**
     * gives the number of given units in an army
     * @param unit
     * @return number of given units
     */
    public int getNumberOfGivenUnits(Unit unit){
        return this.units.stream().filter(x -> x.equals(unit)).toList().size();
    }

    /**
     * gets all distinct units in a set
     * @return HashSet<Unit>
     */
    public HashSet<Unit> getDistinctUnits(){
        HashSet<Unit> distinct = new HashSet<>();
        this.getAllUnits().stream().forEach(unit -> {
            if(!distinct.stream().anyMatch(x -> x.equals(unit))){
                distinct.add(unit);
            }
        });
        return distinct;
    }

    public int getTotalHealth(){
        return this.units.stream().mapToInt(Unit::getHealth).sum();
    }


    /**
     * returns a status of the army as a string
     * @return String
     */
    @Override
    public String toString(){
        String output = "Name of army: " + this.getName() + "\n";
        output += "Number of remaining units: " + this.getSizeOfArmy();
        for(Unit unit : units){
            output += "\n unit name: " + unit.getName() + "\n health: " + unit.getHealth();
        }
        return output;
    }




    /**
     * checks if the two armies are equal
     * @param army
     * @return true if equal, false if not
     */
    public boolean equals(Army army){
        AtomicBoolean equal = new AtomicBoolean(true);
        //checks if armies contain the same amount of each distinct unit type
        this.getDistinctUnits().stream().forEach(x -> {
            if(this.getNumberOfGivenUnits(x) != army.getNumberOfGivenUnits(x)){
                equal.set(false);
            }
        });
        return equal.get();
    }

    // TODO: Hashcode methods.

}
