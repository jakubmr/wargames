package battle;

import units.Unit;

import java.util.Random;

/**
 * class representing a battle of two armies
 */
public class Battle {
    private Army army1;

    private Army army2;

    private Terrain terrain;

    private static Random random = new Random();

    public Battle(Army army1, Army army2, Terrain terrain) throws IllegalArgumentException {
        if(!army1.hasUnits() || !army2.hasUnits()){
            throw new IllegalArgumentException("One of the armies is empty");
        }
        this.army1 = army1;
        this.army2 = army2;
        this.terrain = terrain;
    }


    /**
     * simulation of a battle
     * @return the army that has won
     */
    public Army simulate(){
        boolean army1Starts = army1Starts();
        boolean unit1HasWon = false;
        boolean unit2HasWon = false;
        var unit1 = army1.getRandom();
        var unit2 = army2.getRandom();
        // runs until both armies have units
        while(army1.hasUnits() && army2.hasUnits()){
            // gets a new unit from army1 if the previous has lost
            if(!unit1HasWon){
                unit1 = army1.getRandom();
            }
            // gets a new unit from army2 if the previous has lost
            if(!unit2HasWon){
                unit2 = army2.getRandom();
            }
            //use fight to reveal the winner
            Unit winner = fight(unit1,unit2,army1Starts,terrain);
            if(winner.equals(unit1)){
                army2.remove(unit2);
                army1Starts = false;
                unit1HasWon = true;
                unit2HasWon = false;
            }else{
                army1.remove(unit1);
                army1Starts = true;
                unit1HasWon = false;
                unit2HasWon = true;
            }
        }
        // return the army that has units left
        if(army1.hasUnits()){
            return army1;
        }else{
            return army2;
        }
    }


    /**
     * randomizes the army that starts
     * @return random boolean value
     */
    public static boolean army1Starts(){
        int startingArmy = 1 + random.nextInt(2);
        boolean army1Starts;
        if(startingArmy == 1){
            army1Starts = true;
        }else{
            army1Starts = false;
        }
        return army1Starts;
    }

    /**
     * gets the unit with remaining health
     * @param unit1
     * @param unit2
     * @return winner unit
     */
    public static Unit getWinnerUnit(Unit unit1, Unit unit2){
        if(unit2.getHealth()<1){
            return unit1;
        }else{
            return unit2;
        }
    }

    /**
     * fight simulation between two units
     * @param unit1
     * @param unit2
     * @param army1Starts unit1 starts if true, unit2 else
     * @return the winner unit
     */
    public static Unit fight(Unit unit1, Unit unit2, boolean army1Starts, Terrain terrain){
        if(army1Starts){
            //runs while both units have health
            while (unit1.getHealth() > 0 && unit2.getHealth() > 0) {
                unit1.attack(unit2,terrain);
                //checks if unit2 still has health after a hit
                if(unit2.getHealth() < 1){
                    break;
                }
                unit2.attack(unit1,terrain);
            }
        }else{
            //runs while both units have health
            while (unit1.getHealth() > 0 && unit2.getHealth() > 0) {
                unit2.attack(unit1,terrain);
                //checks if unit1 still has health after a hit
                if(unit1.getHealth() < 1){
                    break;
                }
                unit1.attack(unit2,terrain);
            }
        }
        return getWinnerUnit(unit1,unit2);
    }

}

