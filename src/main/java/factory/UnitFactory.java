package factory;

import units.*;

import java.util.ArrayList;

/**
 * factory class for units
 */
public class UnitFactory {


    public UnitFactory() {
    }

    /**
     * creats new units based on parameter type, name and health
     * @param type
     * @param name
     * @param health
     * @return new Unit
     */
    public Unit createUnit(UnitType type, String name, int health){
        if(name.isEmpty()){
            throw new IllegalArgumentException("Name is Empty!");
        }else if(health < 1){
            throw new IllegalArgumentException("Health is 0 or less!");
        }
        if(type.equals(UnitType.CAVALRY)){
            return new CavalryUnit(name,health);
        }else if(type.equals(UnitType.COMMANDER)){
            return new CommanderUnit(name, health);
        }else if(type.equals(UnitType.INFANTRY)){
            return new InfantryUnit(name, health);
        }else if(type.equals(UnitType.RANGED)){
            return new RangedUnit(name, health);
        }else{
            return null;
        }
    }

    /**
     * creates an arraylist with a given amount of units based on parameters type, name and health.
     * @param type
     * @param name
     * @param health
     * @param n
     * @return ArrayList with new units
     */
    public ArrayList<Unit> createUnitList(UnitType type, String name, int health, int n){
        ArrayList<Unit> list = new ArrayList<>();
        for(int i = 0; i < n; i++){
            list.add(createUnit(type,name,health));
        }
        return list;
    }
}
