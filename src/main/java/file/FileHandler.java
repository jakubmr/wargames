package file;

import battle.Army;
import units.*;

import java.io.*;

/**
 * class containing static methods for file handling army objects
 */

public class FileHandler {
    /**
     * saves the army as a csv file in the armyFiles directory
     */
    public static void saveAsCsvFile(Army army){
        String fileName = army.getName();
        //temporary handling of taken names
        while(nameTaken(fileName)){
            fileName += "(new)";
        }
        FileWriter writer = null;
        try {
            writer = new FileWriter("armyFiles//" + fileName + ".csv");
            StringBuilder sb = new StringBuilder();
            sb.append(fileName + "\n");
            army.getAllUnits().stream().forEach(x -> sb.append(x.toCsvFormat() + "\n"));
            writer.write(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * makes a new army based on info in given csv file
     * @param fileName the file name
     * @return Army
     */
    public static Army getArmyFromCsv(String fileName){
        Army army = new Army(fileName);
        String filePath = "armyFiles//" + fileName + ".csv";
        BufferedReader reader = null;
        String line = null;

        try {
            reader = new BufferedReader(new FileReader(filePath));

            line = reader.readLine();
            while((line = reader.readLine()) != null){
                army.add(getUnitFromCsvLine(line));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return army;
    }

    /**
     * makes a unit based on info given in a csv line
     * @param csvLine containing unit info
     * @return unit
     */
    public static Unit getUnitFromCsvLine(String csvLine) throws Exception {
        if(csvLine.contains(".") || csvLine.split(",").length != 3){
            throw new Exception("Corrupted File");
        }
        String[] unitInfo = csvLine.split(",");
        if(unitInfo[0].equalsIgnoreCase("InfantryUnit")){
            return new InfantryUnit(unitInfo[1],Integer.parseInt(unitInfo[2]));
        }else if(unitInfo[0].equalsIgnoreCase("RangedUnit")){
            return new RangedUnit(unitInfo[1],Integer.parseInt(unitInfo[2]));
        }else if(unitInfo[0].equalsIgnoreCase("CavalryUnit")){
            return new CavalryUnit(unitInfo[1],Integer.parseInt(unitInfo[2]));
        }else if(unitInfo[0].equalsIgnoreCase("CommanderUnit")){
            return new CommanderUnit(unitInfo[1],Integer.parseInt(unitInfo[2]));
        }else{
            throw new Exception("Corrupted file");
        }
    }

    /**
     * checks if a given army name is already used
     * @return true if taken, false if not
     */
    public static boolean nameTaken(String fileName){
        try {
            BufferedReader reader = new BufferedReader(new FileReader("armyFiles//" + fileName + ".csv"));
            return true;
        } catch (FileNotFoundException e) {
            return false;
        }
    }
}
