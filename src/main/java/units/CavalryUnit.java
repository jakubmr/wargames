package units;

import battle.Terrain;

/**
 * class representing a cavalry Unit
 */
public class CavalryUnit extends Unit{

    /**
     * CavalryUnit Constructor
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * simplified constructor
     * @param name
     * @param health
     */
    public CavalryUnit(String name, int health){
        super(name, health, 20, 12);
    }


    /**
     * method for getting current attack bonus, returns 4 the first attack
     * @return int
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        int bonus = 0;
        if(this.getNrOfAttacksDealt() < 1){
            bonus = 4;
        }
        else{
            bonus = 2;
        }
        if(terrain.equals(Terrain.PLAINS)){
            bonus += 1;
        }
        return bonus;
    }

    /**
     * method for getting current resist bonus
     * @return int
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        if(terrain.equals(Terrain.FOREST)){
            return 0;
        }else{
            return 1;
        }
    }

    @Override
    public Unit copyUnit(){
        return new CavalryUnit(this.getName(),this.getHealth(),this.getAttack(),this.getArmor());
    }
}
