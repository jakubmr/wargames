package units;

import battle.Terrain;

/**
 * abstract class representing a Unit
 */
public abstract class Unit {
    /**
     * name of unit
     */
    private String name;

    /**
     * amount of health
     */
    private int health;

    /**
     * attack strength
     */
    private int attack;

    /**
     * strength of armor
     */
    private int armor;

    /**
     * number of attacks dealt
     */
    private int nrOfAttacksDealt = 0;


    /**
     * number of attacks taken
     */

    private int nrOfAttacksTaken = 0;


    /**
     * constructor for Unit class
     * @param name
     * @param health
     * @param attack
     * @param armor
     */

    public Unit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        if(name.isEmpty()){
            throw new IllegalArgumentException("Name is blank");
        }

        if(attack < 0){
            throw new IllegalArgumentException("Attack is negative");
        }

        if(health < 1){
            throw new IllegalArgumentException("Health is less than 1");
        }

        if(armor < 0){
            throw new IllegalArgumentException("Armor is negative");
        }
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * method for attacking
     * @param opponent
     */
    public void attack(Unit opponent, Terrain terrain){
        int attack = this.getAttack() + this.getAttackBonus(terrain);
        int armor = opponent.getArmor() + this.getResistBonus(terrain);

        if(attack > armor){
            int newHealth = opponent.getHealth() + armor - attack;
            opponent.setHealth(newHealth);
        }
        this.nrOfAttacksDealt++;
        opponent.nrOfAttacksTaken++;
    }

    public String getName() {
        return name;
    }

    public int getHealth() {
        return health;
    }

    public int getAttack() {
        return attack;
    }

    public int getArmor() {
        return armor;
    }

    public int getNrOfAttacksDealt() {
        return nrOfAttacksDealt;
    }

    public int getNrOfAttacksTaken() {
        return nrOfAttacksTaken;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public abstract int getAttackBonus(Terrain terrain);

    public abstract int getResistBonus(Terrain terrain);

    public abstract Unit copyUnit();


    /**
     * toString method
     * @return String
     */
    public String toString(){
        String output = "Name: " + this.getName() + "\nHealth: " + this.getHealth() + "\nAttack strength: " + this.getAttack() + "\nArmor strength: " + this.getArmor() + "\nAttacks Dealt: " + this.getNrOfAttacksDealt() + "\nAttacks Taken: " + this.getNrOfAttacksTaken();

        return output;
    }

    /**
     * gives unit info in a csv format
     * @return String in csv format
     */
    public String toCsvFormat(){
        //splits the return of getClass() and gets the part after the period.
        String unitClass = this.getClass().toString().split("\\.")[1];
        String unitName = this.getName();
        String unitHealth = String.valueOf(this.getHealth());
        return unitClass + "," + unitName + "," + unitHealth;
    }


    /**
     * compares every parameter of two units
     * @param o
     * @return true if units are equal
     */
    @Override
    public boolean equals(Object o){
        if(o instanceof Unit){
            return (this.getName().equals(((Unit) o).getName()) && this.getHealth() == ((Unit) o).getHealth()
                    && this.getArmor() == ((Unit) o).getArmor() && this.getAttack() == ((Unit) o).getAttack()
                    && this.getAttackBonus(Terrain.FOREST) == ((Unit) o).getAttackBonus(Terrain.FOREST)
                    && this.getResistBonus(Terrain.FOREST) == ((Unit) o).getResistBonus(Terrain.FOREST)
                    && this.getAttackBonus(Terrain.HILL) == ((Unit) o).getAttackBonus(Terrain.HILL)
                    && this.getResistBonus(Terrain.HILL) == ((Unit) o).getResistBonus(Terrain.HILL)
                    && this.getAttackBonus(Terrain.PLAINS) == ((Unit) o).getAttackBonus(Terrain.PLAINS)
                    && this.getResistBonus(Terrain.PLAINS) == ((Unit) o).getResistBonus(Terrain.PLAINS)
                    && this.getNrOfAttacksDealt() == ((Unit) o).getNrOfAttacksDealt()
                    && this.getNrOfAttacksTaken() == ((Unit) o).getNrOfAttacksTaken());
        }
        return false;
    }
}
