package battle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import units.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ArmyTest {

    Army army1;
    Army army2;
    CavalryUnit cavalry;
    InfantryUnit infantry;
    RangedUnit ranged;
    CommanderUnit commander;

    @BeforeEach
    void init(){
        army1 = new Army("army1");
        army2 = new Army("army2");
        ranged = new RangedUnit("ranged",100);
        cavalry = new CavalryUnit("cavalry",100);
        infantry = new InfantryUnit("infantry",100);
        commander = new CommanderUnit("commander", 100);
    }

    @Test
    void addMethodTest(){
        army1.add(infantry);
        army1.add(infantry);
        army1.add(infantry);
        army1.add(ranged);
        army1.add(cavalry);

        assertEquals(5,army1.getSizeOfArmy());
        assertEquals(3,army1.getInfantryUnits().size());
        assertEquals(cavalry,army1.getAllUnits().get(4));
    }

    @Test
    void addAllMethodTest(){
        ArrayList<Unit> list = new ArrayList<>();

        list.add(cavalry);
        list.add(cavalry);
        list.add(cavalry);
        list.add(infantry);
        list.add(infantry);

        army1.addAll(list);

        assertTrue(army1.getAllUnits().get(0).equals(cavalry));
        assertTrue(army1.getAllUnits().get(3).equals(infantry));
    }

    @Test
    void removeMethodTest(){

        army1.add(cavalry);
        army1.add(cavalry);
        army1.add(infantry);
        army1.add(infantry);
        army1.remove(cavalry);
        assertEquals(3, army1.getSizeOfArmy());
    }

    @Test
    void addNumberOfUnitsTest(){
        army1.addNumberOfUnits(5,cavalry);
        army2.addNumberOfUnits(5,cavalry);
        assertEquals(5,army1.getSizeOfArmy());
        assertEquals(5,army1.getCavalryUnits().size());
    }

    @Test
    void getInfantryTest(){
        army1.add(cavalry);
        army1.add(cavalry);
        army1.add(cavalry);
        army1.add(commander);
        assertEquals(0,army1.getInfantryUnits().size());
        army1.add(infantry);
        army1.add(infantry);
        assertEquals(2,army1.getInfantryUnits().size());
    }

    @Test
    void getRangedTest(){
        army1.add(cavalry);
        army1.add(cavalry);
        army1.add(cavalry);
        army1.add(commander);
        assertEquals(0,army1.getRangedUnits().size());
        army1.add(ranged);
        army1.add(ranged);
        army1.add(ranged);
        assertEquals(3,army1.getRangedUnits().size());
    }

    @Test
    void getCavalryTest(){
        army1.add(ranged);
        army1.add(ranged);
        army1.add(ranged);
        assertEquals(0,army1.getCavalryUnits().size());
        army1.add(cavalry);
        army1.add(cavalry);
        army1.add(cavalry);
        army1.add(commander);
        assertEquals(3,army1.getCavalryUnits().size());
    }

    @Test
    void getCommanderTest(){
        army1.add(cavalry);
        army1.add(cavalry);
        army1.add(cavalry);
        assertEquals(0,army1.getCommanderUnits().size());
        army1.add(commander);
        army1.add(commander);
        assertEquals(2,army1.getCommanderUnits().size());
    }

    @Test
    void getDistinctUnitsTest(){
        army1.add(infantry);
        assertEquals(1, army1.getDistinctUnits().size());
        army1.add(infantry);
        army1.add(ranged);
        assertEquals(2,army1.getDistinctUnits().size());
        army1.add(new InfantryUnit("Test", 50));
        assertEquals(3,army1.getDistinctUnits().size());
    }

    @Test
    void getNumberOfGivenUnitTest(){
        army1.addNumberOfUnits(100,infantry);
        army1.add(cavalry);
        assertEquals(100,army1.getNumberOfGivenUnits(infantry));
        assertEquals(1,army1.getNumberOfGivenUnits(cavalry));
        assertEquals(0,army1.getNumberOfGivenUnits(ranged));
    }
    @Test
    void getTotalHealthTest(){
        army1.add(new RangedUnit("Ranged", 100));
        army1.add(new RangedUnit("Ranged", 100));
        army1.add(new RangedUnit("Ranged", 100));
        assertEquals(300, army1.getTotalHealth());
    }





    @Test
    void equalsMethodTest(){
        army1.add(infantry);
        army1.add(infantry);
        army1.add(ranged);
        army1.add(ranged);
        army1.add(ranged);

        army2.add(ranged);
        army2.add(ranged);
        army2.add(ranged);
        army2.add(infantry);
        army2.add(infantry);
        assertTrue(army1.equals(army2));

        army1.remove(infantry);
        assertFalse(army1.equals(army2));

        army1.add(infantry);
        assertTrue(army1.equals(army2));

        army1.add(infantry);
        assertFalse(army1.equals(army2));
    }
}