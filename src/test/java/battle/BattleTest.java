package battle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import units.CavalryUnit;
import units.InfantryUnit;
import units.Unit;

class BattleTest {

    Army army1;
    Army army2;
    Battle battle;
    Unit cavalry;
    Unit infantry;

    @BeforeEach
    void init() {
        army1 = new Army("army1");
        army2 = new Army("army2");
        cavalry = new CavalryUnit("cavalry", 30);
        infantry = new InfantryUnit("infantry", 30);
        army1.add(cavalry);
        army1.add(infantry);
        army2.add(cavalry);
        army2.add(infantry);
        battle = new Battle(army1, army2,Terrain.FOREST);

    }


    @Test
    void fightMethodTest() {

    }
}