package file;

import battle.Army;
import org.junit.jupiter.api.Test;
import units.InfantryUnit;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class FileHandlerTest {
    @Test
    void saveAsCsvFileTest(){
        Army army1 = new Army("army1");
        army1.addNumberOfUnits(100,(new InfantryUnit("infantry",100)));
        FileHandler.saveAsCsvFile(army1);
        assertTrue(army1.equals(FileHandler.getArmyFromCsv("army1")));
    }

    @Test
    void getUnitFromCsvLine() throws Exception {
        assertThrows(Exception.class, () -> FileHandler.getUnitFromCsvLine("file.WithPer.iod"));
        assertThrows(Exception.class, () -> FileHandler.getUnitFromCsvLine("fileWithPeriod"));
        assertThrows(Exception.class, () -> FileHandler.getUnitFromCsvLine("I"));
        InfantryUnit infantry = new InfantryUnit("test", 50);
        assertTrue(infantry.equals(FileHandler.getUnitFromCsvLine("InfantryUnit,test,50")));
    }

    @Test
    void nameTakenTest(){
        assertTrue(FileHandler.nameTaken("testArmy"));
        assertFalse(FileHandler.nameTaken("test---123321---test"));
    }

}