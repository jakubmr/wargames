package units;

import battle.Terrain;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommanderUnitTest {

    @Test
    void commanderUnitConstructorHealthTest(){
        var commander = new CommanderUnit("eks", 100);
        assertEquals(100,commander.getHealth());
    }

    @Test
    void commanderUnitCostructorAttackTest(){
        var commander = new CommanderUnit("eks", 100);
        assertEquals(25,commander.getAttack());
    }

    @Test
    void commanderUnitConstructorArmorTest(){
        var commander = new CommanderUnit("eks", 100);
        assertEquals(15,commander.getArmor());
    }

    @Test
    void commanderUnitAttackBonusAfterNoAttackTest(){
        var commander = new CommanderUnit("eks", 100);
        assertEquals(5,commander.getAttackBonus(Terrain.PLAINS));
        assertEquals(4,commander.getAttackBonus(Terrain.HILL));
        assertEquals(4,commander.getAttackBonus(Terrain.FOREST));
    }

    @Test
    void commanderUnitAttackBonusAfterOneAttackTest(){
        var commander1 = new CommanderUnit("eks1", 100);
        var commander2 = new CommanderUnit("eks2", 100);
        commander1.attack(commander2,Terrain.PLAINS);
        assertEquals(3,commander1.getAttackBonus(Terrain.PLAINS));
        assertEquals(2,commander1.getAttackBonus(Terrain.HILL));
        assertEquals(2,commander1.getAttackBonus(Terrain.FOREST));
    }

    @Test
    void commanderUnitResistBonusAfterNoAttackTest(){
        var commander = new CommanderUnit("eks", 100);
        assertEquals(0, commander.getResistBonus(Terrain.FOREST));
        assertEquals(1, commander.getResistBonus(Terrain.PLAINS));
        assertEquals(1, commander.getResistBonus(Terrain.HILL));
    }

    @Test
    void commanderUnitResistBonusAfterOneAttackTest(){
        var commander1 = new CommanderUnit("eks1", 100);
        var commander2 = new CommanderUnit("eks2", 100);
        commander1.attack(commander2,Terrain.FOREST);
        assertEquals(0,commander2.getResistBonus(Terrain.FOREST));
        assertEquals(1,commander2.getResistBonus(Terrain.HILL));
        assertEquals(1,commander2.getResistBonus(Terrain.PLAINS));
    }
}