package units;

import battle.Terrain;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InfantryUnitTest {

    @Test
    void InfantryConstructorHealthTest(){
        var infantry = new InfantryUnit("eks", 100);
        assertEquals(100, infantry.getHealth());
    }

    @Test
    void infantryConstructorAttackTest(){
        var infantry = new InfantryUnit("eks", 100);
        assertEquals(15,infantry.getAttack());
    }


    @Test
    void infantryConstructorArmorTest(){
        var infantry = new InfantryUnit("eks", 100);
        assertEquals(10, infantry.getArmor());
    }

    @Test
    void infantryAttackBonusTest(){
        var infantry = new InfantryUnit("eks", 100);
        assertEquals(3, infantry.getAttackBonus(Terrain.FOREST));
        assertEquals(2, infantry.getAttackBonus(Terrain.HILL));
        assertEquals(2, infantry.getAttackBonus(Terrain.PLAINS));
    }

    @Test
    void infantryResistBonusTest(){
        var infantry = new InfantryUnit("eks", 100);
        assertEquals(2,infantry.getResistBonus(Terrain.FOREST));
        assertEquals(1,infantry.getResistBonus(Terrain.HILL));
        assertEquals(1,infantry.getResistBonus(Terrain.PLAINS));
    }
}