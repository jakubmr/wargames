package units;

import battle.Terrain;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangedUnitTest {

    @Test
    void RangedUnitAttackBonusTest(){
        var ranger1 = new RangedUnit("eks1", 100);
        assertEquals(5,ranger1.getAttackBonus(Terrain.HILL));
        assertEquals(2,ranger1.getAttackBonus(Terrain.FOREST));
        assertEquals(3,ranger1.getAttackBonus(Terrain.PLAINS));
    }

    @Test
    void RangedUnitResistBonusAfterNoAttacks(){
        var ranger1 = new RangedUnit("eks1", 100);
        assertEquals(6,ranger1.getResistBonus(Terrain.HILL));
        assertEquals(6,ranger1.getResistBonus(Terrain.FOREST));
        assertEquals(6,ranger1.getResistBonus(Terrain.PLAINS));
    }

    @Test
    void RangedUnitResistBonusAfterOneAttack(){
        var ranger1 = new RangedUnit("eks1", 100);
        var ranger2 = new RangedUnit("eks2", 100);
        ranger1.attack(ranger2,Terrain.HILL);
        assertEquals(4,ranger2.getResistBonus(Terrain.HILL));
        assertEquals(4,ranger2.getResistBonus(Terrain.PLAINS));
        assertEquals(4,ranger2.getResistBonus(Terrain.FOREST));
    }

    @Test
    void RangedUnidResistBonusAfterTwoAttacks(){
        var ranger1 = new RangedUnit("eks1", 100);
        var ranger2 = new RangedUnit("eks2", 100);
        ranger1.attack(ranger2,Terrain.HILL);
        ranger1.attack(ranger2,Terrain.FOREST);
        assertEquals(2,ranger2.getResistBonus(Terrain.PLAINS));
        assertEquals(2,ranger2.getResistBonus(Terrain.FOREST));
        assertEquals(2,ranger2.getResistBonus(Terrain.HILL));
    }
}