package units;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UnitTest {
    @Test
    void equalsMethodTest(){
        InfantryUnit infantry = new InfantryUnit("Infantry",100);
        InfantryUnit infantry1 = new InfantryUnit("Infantry",100);
        InfantryUnit infantry2 = new InfantryUnit("Infantry2",100);
        InfantryUnit infantry3 = new InfantryUnit("Infantry",99);

        assertTrue(infantry.equals(infantry1));
        assertTrue(infantry.equals(infantry.copyUnit()));
        assertFalse(infantry.equals(infantry2));
        assertFalse(infantry.equals(infantry3));
    }

}